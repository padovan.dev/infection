# infection

## Model
* two dimensions world *X*,*Y*
* *N* people randomly disposed
    * Population density  _d=N/(X*Y)_
* every person can be: **S**usceptible, **I**nfectious, **R**ecovered
* a recoverd person can't infectious again
* death rate is 0
* the world evolve at round, every round:
    * a virus can infect people closer than *r* to an infected person with a probability *i*
    * every infected person heals after *t* round
    * each person moves in the world, both vertically and horizontally, by a random value between -m and +m; if the movement goes beyond the boundaries of the world it returns to the other side
       * it follows that, in one turn, an infected person surrounded by healthy people will infect on average c = d * r^2 * PI * k individuals

## Software and GUI

### Input
Population: N
World size: X=Y, it's a square
Avg recovery per round: t
Neighbour limit: r
Contagions per round: expected number c of contagions per round by an infected person surrounded by susceptible people → i = c / (d *  r ^ 2 * PI)
Move index: m. In this version it is the only parameter that can be modified during execution
### Output
Infects: 
New infects: 
New Infects %: 
Recovers: 
Image with simulation: Blu=Susceptible, Red=Infectious, Green=recovered