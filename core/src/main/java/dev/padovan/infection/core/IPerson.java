package dev.padovan.infection.core;

import java.util.List;

public interface IPerson {

	List<Integer> getContaminations();

	Integer getLastContamination();

	boolean isDead();

	boolean isRecover();

	boolean isInfected();

	double getY();

	double getX();

	long getId();

	List<Integer> getRecoveries();

	Integer getLastRecovery();

}
