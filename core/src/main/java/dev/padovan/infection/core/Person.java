package dev.padovan.infection.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Person implements IPerson{

	public enum Effect {
		RECOVERY,
		DEATH,
		NONE
	}

	private final long id;

	private double x;
	private double y;
	private boolean infected;
	private boolean dead;
	private final List<Integer> contaminations = new ArrayList<>();
	private final List<Integer> recoveries = new ArrayList<>();

	public Person(final long id, final double x, final double y, final boolean infect) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.infected = infect;
		if (infect) {
			this.contaminations.add(Integer.valueOf(0));
		}
		this.dead = false;
	}

	@Override
	public final long getId() {
		return this.id;
	}

	@Override
	public final double getX() {
		return this.x;
	}

	@Override
	public final double getY() {
		return this.y;
	}

	@Override
	public final boolean isInfected() {
		return !this.dead && this.infected;
	}

	@Override
	public final boolean isRecover() {
		return !this.dead && !this.infected && !this.contaminations.isEmpty();
	}

	@Override
	public boolean isDead() {
		return this.dead;
	}

	public synchronized final boolean infect(final Virus virus, final int round) {
		if (this.dead) {
			throw new RuntimeException(this.id + " is dead");
		}
		if (this.infected) {
			return false;
		}
		if (!virus.isRepeatable() && !this.contaminations.isEmpty()) {
			return false;
		}
		if (this.infectDecision(virus, round)) {
			this.infected = true;
			this.contaminations.add(Integer.valueOf(round));
		}
		return this.infected;
	}

	public synchronized final Effect effect(final Virus virus, final int round) {
		if (this.dead) {
			throw new RuntimeException(this.id + " is dead");
		}
		if (!this.infected) {
			return Effect.NONE;
		}
		if (this.recoveryDecision(virus, round)) {
			this.infected = false;
			this.recoveries.add(Integer.valueOf(round));
			return Effect.RECOVERY;
		}
		if (this.deathDecision(virus, round)) {
			this.dead = true;
			return Effect.DEATH;
		}
		return Effect.NONE;
	}

	public synchronized void move(final double maxX, final double maxY) {
		if (this.dead) {
			throw new RuntimeException(this.id + " is dead");
		}
		final double newX = this.getNewX(maxX);
		if (newX > maxX || newX < 0) {
			throw new IllegalStateException("New x is " + newX);
		}
		this.x = newX;

		final double newY = this.getNewY(maxY);
		if (newY > maxY || newY < 0) {
			throw new IllegalStateException("New y is " + newY);
		}
		this.y = newY;
	}

	@Override
	public Integer getLastContamination() {
		if (this.contaminations.isEmpty()) {
			return null;
		}
		return this.contaminations.get(this.contaminations.size() - 1);
	}

	@Override
	public List<Integer> getContaminations() {
		return Collections.unmodifiableList(this.contaminations);
	}

	@Override
	public Integer getLastRecovery() {
		if (this.recoveries.isEmpty()) {
			return null;
		}
		return this.recoveries.get(this.recoveries.size() - 1);
	}

	@Override
	public List<Integer> getRecoveries() {
		return Collections.unmodifiableList(this.recoveries);
	}

	protected abstract double getNewX(final double maxX);

	protected abstract double getNewY(final double maxY);

	protected abstract boolean infectDecision(final Virus virus, final int round);

	protected abstract boolean deathDecision(final Virus virus, final int round);

	protected abstract boolean recoveryDecision(final Virus virus, final int round);

}
