package dev.padovan.infection.core;

public interface Virus {

	double getInfectiousness();

	int getAverageRecoveryRound();

	boolean isRepeatable();

	double getDeathRate();

	double getNeighbourLimit();

}