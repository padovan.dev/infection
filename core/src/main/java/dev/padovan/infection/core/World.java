package dev.padovan.infection.core;

import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import dev.padovan.infection.core.Person.Effect;
import dev.padovan.infection.core.events.EffectEvent;
import dev.padovan.infection.core.events.EffectsEvent;
import dev.padovan.infection.core.events.InfectionEvent;
import dev.padovan.infection.core.events.InfectionsEvent;
import dev.padovan.infection.core.events.MoveEvent;
import dev.padovan.infection.core.events.RoundEvent;

public class World {

	private final double width;
	private final double height;

	private int round;
	private final Set<Person> people;
	private final Virus virus;

	private List<Consumer<InfectionEvent>> infectionListeners;
	private List<Consumer<InfectionsEvent>> infectionsListeners;
	private List<Consumer<EffectEvent>> effectListeners;
	private List<Consumer<EffectsEvent>> effectsListeners;
	private List<Consumer<MoveEvent>> moveListeners;
	private List<Consumer<RoundEvent>> endRoundListeners;

	public World(final Set<Person> people, final Virus virus, final int width, final int height) {
		this.round = 0;
		this.people = people;
		this.virus = virus;
		this.width = width;
		this.height = height;
	}

	public double getWidth() {
		return this.width;
	}

	public double getHeight() {
		return this.height;
	}

	public Set<IPerson> getPeople() {
		return new HashSet<>(this.people);
	}

	public long infected() {
		return this.people.stream().filter(Person::isInfected).count();
	}

	public long recovered() {
		return this.people.stream().filter(Person::isRecover).count();
	}

	public void round() {
		final Set<IPerson> newInfects = this.infect();
		if (this.infectionsListeners != null && !newInfects.isEmpty()) {
			final InfectionsEvent event = new InfectionsEvent(Collections.unmodifiableSet(newInfects), this.round);
			for (final Consumer<InfectionsEvent> consumer : this.infectionsListeners) {
				consumer.accept(event);
			}
		}

		final Set<IPerson> effect = this.effect();
		if (this.effectsListeners != null) {
			for (final Consumer<EffectsEvent> consumer : this.effectsListeners) {
				final EffectsEvent effectsEvent = new EffectsEvent(effect, this.round);
				consumer.accept(effectsEvent);
			}
		}

		this.move();

		if (this.endRoundListeners != null) {
			final RoundEvent roundEvent = new RoundEvent(this);
			for (final Consumer<RoundEvent> consumer : this.endRoundListeners) {
				consumer.accept(roundEvent);
			}
		}
		this.round++;
	}

	public int getRound() {
		return this.round;
	}

	public void addInfectionListener(final Consumer<InfectionEvent> infectionListenter) {
		if (this.infectionListeners == null) {
			this.infectionListeners = new ArrayList<>();
		}
		this.infectionListeners.add(infectionListenter);
	}

	public void removeInfectionListener(final Consumer<InfectionEvent> infectionListenter) {
		if (this.infectionListeners == null) {
			return;
		}
		this.infectionListeners.remove(infectionListenter);
		if (this.infectionListeners.isEmpty()) {
			this.infectionListeners = null;
		}
	}

	public void addInfectionsListener(final Consumer<InfectionsEvent> infectionsListenter) {
		if (this.infectionsListeners == null) {
			this.infectionsListeners = new ArrayList<>();
		}
		this.infectionsListeners.add(infectionsListenter);
	}

	public void removeInfectionsListener(final Consumer<InfectionsEvent> infectionsListenter) {
		if (this.infectionsListeners == null) {
			return;
		}
		this.infectionsListeners.remove(infectionsListenter);
		if (this.infectionsListeners.isEmpty()) {
			this.infectionsListeners = null;
		}
	}

	public void addEffectListener(final Consumer<EffectEvent> effectListener) {
		if (this.effectListeners == null) {
			this.effectListeners = new ArrayList<>();
		}
		this.effectListeners.add(effectListener);
	}

	public void removeEffectListener(final Consumer<EffectEvent> effectListener) {
		if (this.effectListeners == null) {
			return;
		}
		this.effectListeners.remove(effectListener);
		if (this.effectListeners.isEmpty()) {
			this.effectListeners = null;
		}
	}

	public void addEffectsListener(final Consumer<EffectsEvent> effectsListener) {
		if (this.effectsListeners == null) {
			this.effectsListeners = new ArrayList<>();
		}
		this.effectsListeners.add(effectsListener);
	}

	public void removeEffectsListener(final Consumer<EffectsEvent> effectsListener) {
		if (this.effectsListeners == null) {
			return;
		}
		this.effectsListeners.remove(effectsListener);
		if (this.effectsListeners.isEmpty()) {
			this.effectsListeners = null;
		}
	}

	public void addMoveListener(final Consumer<MoveEvent> moveListener) {
		if (this.moveListeners == null) {
			this.moveListeners = new ArrayList<>();
		}
		this.moveListeners.add(moveListener);
	}

	public void removeMoveListener(final Consumer<MoveEvent> moveListener) {
		if (this.moveListeners == null) {
			return;
		}
		this.moveListeners.remove(moveListener);
		if (this.moveListeners.isEmpty()) {
			this.moveListeners = null;
		}
	}

	public void addEndRoundListener(final Consumer<RoundEvent> effectsListener) {
		if (this.endRoundListeners == null) {
			this.endRoundListeners = new ArrayList<>();
		}
		this.endRoundListeners.add(effectsListener);
	}

	public void removeEndRoundListener(final Consumer<RoundEvent> effectsListener) {
		if (this.endRoundListeners == null) {
			return;
		}
		this.endRoundListeners.remove(effectsListener);
		if (this.endRoundListeners.isEmpty()) {
			this.endRoundListeners = null;
		}
	}

	private Set<IPerson> infect() {
		final Set<Person> infects = this.people.stream().filter(Person::isInfected).collect(Collectors.toSet());
		final Map<Integer, List<Person>> partitions = this.people.stream().filter(p -> !p.isInfected()).collect(groupingBy(p -> Integer.valueOf((int) Math.round(p.getX()))));
		final Set<IPerson> newInfects = new HashSet<>();
		for (final IPerson infect : infects) {
			final double neighbourLimit = this.virus.getNeighbourLimit();
			final int minPart = (int) Math.floor(infect.getX() - neighbourLimit);
			final int maxPart = (int) Math.ceil(infect.getX() + neighbourLimit);

			final List<Person> neighbours = new ArrayList<>();
			final int min = Math.max(0, minPart);
			final int max = Math.min((int) Math.round(this.height + 1), maxPart);
			for (int i = min; i <= max; i++) {
				final List<Person> partition = partitions.get(Integer.valueOf(i));
				if (partition != null) {
					final List<Person> partitionNeighbours = partition.stream().filter(p -> this.isNeighbour(p, infect)).collect(Collectors.toList());
					neighbours.addAll(partitionNeighbours);
				}
			}

			for (final Person person : neighbours) {
				final boolean isInfect = person.infect(this.virus, this.round);
				if (isInfect) {
					newInfects.add(person);
					if (this.infectionListeners != null) {
						for (final Consumer<InfectionEvent> c : this.infectionListeners) {
							c.accept(new InfectionEvent(person, this.round));
						}
					}
				}
			}
		}
		return newInfects;
	}

	private void move() {
		for (final Person person : this.people) {
			final double oldX = person.getX();
			final double oldY = person.getY();
			person.move(this.width, this.height);
			if (this.moveListeners != null) {
				final MoveEvent moveEvent = new MoveEvent(person, oldX, oldY);
				for (final Consumer<MoveEvent> consumer : this.moveListeners) {
					consumer.accept(moveEvent);
				}
			}
		}
	}

	private Set<IPerson> effect() {
		final Set<Person> infects = this.people.stream().filter(Person::isInfected).collect(Collectors.toSet());
		for (final Person person : infects) {
			final Effect effect = person.effect(this.virus, this.round);
			if (effect == Effect.DEATH) {
				this.people.remove(person);
			}
			if (this.effectListeners != null) {
				for (final Consumer<EffectEvent> consumer : this.effectListeners) {
					consumer.accept(new EffectEvent(person, effect, this.round));
				}
			}
		}
		return new HashSet<>(infects);
	}

	private boolean isNeighbour(final IPerson p1, final IPerson p2) {
		if (p1 == p2) {
			return false;
		}
		final double dx = p1.getX() - p2.getX();
		final double neighbourLimit = this.virus.getNeighbourLimit();
		if (dx > neighbourLimit) {
			return false;
		}
		final double dy = p1.getY() - p2.getY();
		if (dy > neighbourLimit) {
			return false;
		}
		final double distance = Math.sqrt(dx * dx + dy * dy);
		final boolean isNeighbour = distance < neighbourLimit;
		return isNeighbour;

	}

}
