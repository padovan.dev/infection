package dev.padovan.infection.core.events;

import dev.padovan.infection.core.IPerson;
import dev.padovan.infection.core.Person.Effect;

public class EffectEvent {

	private final IPerson person;
	private final Effect effect;
	private final int round;

	public EffectEvent(final IPerson person, final Effect effect, final int round) {
		this.person = person;
		this.effect = effect;
		this.round = round;
	}

	public IPerson getPerson() {
		return this.person;
	}

	public Effect getEffect() {
		return this.effect;
	}

	public int getRound() {
		return this.round;
	}

}
