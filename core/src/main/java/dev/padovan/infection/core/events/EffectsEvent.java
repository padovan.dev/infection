package dev.padovan.infection.core.events;

import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.Set;

import dev.padovan.infection.core.IPerson;

public class EffectsEvent {

	private final Set<IPerson> noneEffect;
	private final Set<IPerson> recovered;
	private final Set<IPerson> deads;
	private final int round;

	public EffectsEvent(final Set<IPerson> infected, final int round) {
		this.noneEffect = Collections.unmodifiableSet(infected.stream().filter(IPerson::isInfected).collect(toSet()));
		this.recovered = Collections.unmodifiableSet(infected.stream().filter(p -> !p.isDead() && !p.isInfected()).collect(toSet()));
		this.deads = Collections.unmodifiableSet(infected.stream().filter(IPerson::isDead).collect(toSet()));
		this.round = round;
	}

	public Set<IPerson> getNoneEffect() {
		return this.noneEffect;
	}

	public Set<IPerson> getRecovered() {
		return this.recovered;
	}

	public Set<IPerson> getDeads() {
		return this.deads;
	}

	public int getRound() {
		return this.round;
	}

}
