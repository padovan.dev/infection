package dev.padovan.infection.core.events;

import dev.padovan.infection.core.IPerson;

public class InfectionEvent {

	private final IPerson person;
	private final int round;

	public InfectionEvent(final IPerson person, final int round) {
		this.person = person;
		this.round = round;
	}

	public IPerson getPerson() {
		return this.person;
	}

	public int getRound() {
		return this.round;
	}

}
