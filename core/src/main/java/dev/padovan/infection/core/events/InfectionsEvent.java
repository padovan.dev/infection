package dev.padovan.infection.core.events;

import java.util.Set;

import dev.padovan.infection.core.IPerson;

public class InfectionsEvent {

	private final Set<IPerson> people;
	private final int round;

	public InfectionsEvent(final Set<IPerson> people, final int round) {
		this.people = people;
		this.round = round;
	}

	public Set<IPerson> getPeople() {
		return this.people;
	}

	public int getRound() {
		return this.round;
	}

}
