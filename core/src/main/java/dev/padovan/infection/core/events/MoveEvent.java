package dev.padovan.infection.core.events;

import dev.padovan.infection.core.IPerson;

public class MoveEvent {

	private final IPerson person;
	private final double oldX;
	private final double oldY;

	public MoveEvent(final IPerson person, final double oldX, final double oldY) {
		this.person = person;
		this.oldX = oldX;
		this.oldY = oldY;
	}

	public IPerson getPerson() {
		return this.person;
	}

	public double getOldX() {
		return this.oldX;
	}

	public double getOldY() {
		return this.oldY;
	}

}
