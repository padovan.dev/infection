package dev.padovan.infection.core.events;

import dev.padovan.infection.core.World;

public class RoundEvent {

	private final World world;

	public RoundEvent(final World world) {
		this.world = world;
	}

	public World getWorld() {
		return this.world;
	}
}
