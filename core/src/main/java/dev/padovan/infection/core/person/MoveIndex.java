package dev.padovan.infection.core.person;

public class MoveIndex {

	private double moveIndex;

	public double getMoveIndex() {
		return this.moveIndex;
	}

	public void setMoveIndex(final double moveIndex) {
		this.moveIndex = moveIndex;
	}

}
