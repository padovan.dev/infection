package dev.padovan.infection.core.person;

import dev.padovan.infection.core.Person;
import dev.padovan.infection.core.Virus;

public class SimpleEternalPerson extends Person {

	private final MoveIndex moveIndex;

	public SimpleEternalPerson(final long id, final double x, final double y, final boolean infect, final MoveIndex moveIndex) {
		super(id, x, y, infect);
		this.moveIndex = moveIndex;
	}

	@Override
	protected double getNewX(final double maxX) {
		final double _x = this.getX() + (Math.random() - 0.5) * this.moveIndex.getMoveIndex();
		if (_x > maxX) {
			return _x - maxX;
		}
		if (_x < 0) {
			return _x + maxX;
		}
		return _x;
	}

	@Override
	protected double getNewY(final double maxY) {
		final double _y = this.getY() + (Math.random() - 0.5) * this.moveIndex.getMoveIndex();
		if (_y > maxY) {
			return _y - maxY;
		}
		if (_y < 0) {
			return _y + maxY;
		}
		return _y;

	}

	@Override
	protected boolean infectDecision(final Virus virus, final int round) {
		return Math.random() < virus.getInfectiousness();
	}

	@Override
	protected boolean recoveryDecision(final Virus virus, final int round) {
		return round - this.getLastContamination().intValue() >= virus.getAverageRecoveryRound();
	}

	@Override
	protected boolean deathDecision(final Virus virus, final int round) {
		return Math.random() < virus.getDeathRate();
	}

}
