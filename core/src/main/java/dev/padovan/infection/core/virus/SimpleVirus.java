package dev.padovan.infection.core.virus;

import dev.padovan.infection.core.Virus;

public class SimpleVirus implements Virus {

	private final double infectiousness;
	private final int averageRecoveryRound;
	private final boolean repeatable;
	private final double deathRate;
	private final double neighbourLimit;

	public SimpleVirus(final double infectiousness, final int averageRecoveryRound, final boolean repeatable, final double deathRate, final double neighbourLimit) {
		this.infectiousness = infectiousness;
		this.averageRecoveryRound = averageRecoveryRound;
		this.repeatable = repeatable;
		this.deathRate = deathRate;
		this.neighbourLimit = neighbourLimit;
	}

	@Override
	public double getInfectiousness() {
		return this.infectiousness;
	}

	@Override
	public int getAverageRecoveryRound() {
		return this.averageRecoveryRound;
	}

	@Override
	public boolean isRepeatable() {
		return this.repeatable;
	}

	@Override
	public double getDeathRate() {
		return this.deathRate;
	}

	@Override
	public double getNeighbourLimit() {
		return this.neighbourLimit;
	}

}
