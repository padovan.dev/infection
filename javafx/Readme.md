# Download javafx libs
download javafx sdk 11 from here https://gluonhq.com/products/javafx/ 

## Linux:

	wget http://gluonhq.com/download/javafx-11-0-2-sdk-linux/ -O javafx-11-0-2-sdk-linux.zip
	unzip javafx-11-0-2-sdk-linux.zip 
	mv javafx-sdk-11.0.2/ lib/
	rm javafx-11-0-2-sdk-linux.zip 