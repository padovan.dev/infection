package com.azserve.azframework.common.funct;

@FunctionalInterface
public interface ObjIntFunction<T, R> {

	R apply(T t, int value);

}
