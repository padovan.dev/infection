package com.azserve.azframework.javafx.components;

import javafx.beans.property.Property;

public interface Bindable<W, T> {

	Property<W> valueProperty();

	void setReadOnly(boolean readOnly);

	default W getValue() {
		return this.valueProperty().getValue();
	}

	default void setValue(final W value) {
		this.valueProperty().setValue(value);
	}

	default T getUnwrappedValue() {
		return this.unwrapValue(this.getValue());
	}

	default void setUnwrappedValue(final T value) {
		this.setValue(this.wrapValue(value));
	}

	default void clearValue() {
		this.setUnwrappedValue(null);
	}

	W wrapValue(T value);

	T unwrapValue(W wrappedValue);
}
