package com.azserve.azframework.javafx.components;

import java.util.Objects;

import com.azserve.azframework.common.lang.NumberUtils;
import com.azserve.azframework.common.lang.StringUtils;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;

public class IntegerField extends ValueField<Integer> {

	private final SimpleObjectProperty<Integer> nullValueProperty = new SimpleObjectProperty<>(this, "nullValue", null);
	private final SimpleIntegerProperty minValueProperty = new SimpleIntegerProperty(this, "minValue", Integer.MIN_VALUE);
	private final SimpleIntegerProperty maxValueProperty = new SimpleIntegerProperty(this, "maxValue", Integer.MAX_VALUE);

	public IntegerField() {
		this(-1);
	}

	public IntegerField(final int cols) {
		super();

		if (cols >= 0) {
			this.prefColumnCountProperty().set(cols);
		}

		this.initDefaults();
	}

	public IntegerField(final Integer value) {
		this(value, -1);
	}

	public IntegerField(final Integer value, final int cols) {
		this(cols);
		this.setValue(value);
	}

	private void initDefaults() {
		this.setAlignment(Pos.CENTER_RIGHT);

		this.nullValueProperty.addListener((obs, old, value) -> {
			this.setValue(IntegerField.fix(this.getValue(), value, value.intValue(), this.getMaxValue()));
		});

		this.minValueProperty.addListener((obs, old, value) -> {
			this.setValue(IntegerField.fix(this.getValue(), this.getNullValue(), value.intValue(), this.getMaxValue()));
		});

		this.maxValueProperty.addListener((obs, old, value) -> {
			this.setValue(IntegerField.fix(this.getValue(), this.getNullValue(), this.getMinValue(), value.intValue()));
		});
	}

	@Override
	protected boolean checkUserInput(final String text) {
		if (StringUtils.isNullOrEmpty(text)) {
			return true;
		}
		if (text.equals("-")) {
			return this.minValueProperty.get() < 0;
		}
		return this.fromString(text) != null;
	}

	@Override
	protected String toString(final Integer object) {
		return Objects.toString(object, "");
	}

	@Override
	protected Integer fromString(final String string) {
		final Integer integerValue = NumberUtils.parseInteger(string);
		if (integerValue != null) {
			final int value = integerValue.intValue();
			if (this.minValueProperty.get() > value) {
				return null;
			}
			if (this.maxValueProperty.get() < value) {
				return null;
			}
		}
		return integerValue;
	}

	public SimpleObjectProperty<Integer> nullValueProperty() {
		return this.nullValueProperty;
	}

	public Integer getNullValue() {
		return this.nullValueProperty.get();
	}

	public void setNullValue(final Integer nullValue) {
		this.nullValueProperty.set(nullValue);
	}

	public SimpleIntegerProperty minValueProperty() {
		return this.minValueProperty;
	}

	public int getMinValue() {
		return this.minValueProperty.get();
	}

	public void setMinValue(final int minValue) {
		this.minValueProperty.set(minValue);
	}

	public SimpleIntegerProperty maxValueProperty() {
		return this.maxValueProperty;
	}

	public int getMaxValue() {
		return this.maxValueProperty.get();
	}

	public void setMaxValue(final int maxValue) {
		this.maxValueProperty.set(maxValue);
	}

	@Override
	protected Integer fix(final Integer value) {
		return IntegerField.fix(value, this.nullValueProperty.get(), this.minValueProperty.get(), this.maxValueProperty.get());
	}

	private static Integer fix(final Integer value, final Integer nullValue, final int minValue, final int maxValue) {
		if (value != null) {
			final int i = value.intValue();
			if (i > maxValue || i < minValue) {
				return null;
			}
		} else if (nullValue != null) {
			final int i = nullValue.intValue();
			if (i > maxValue) {
				return Integer.valueOf(maxValue);
			}
			if (i < minValue) {
				return Integer.valueOf(minValue);
			}
			return nullValue;
		}
		return value;
	}
}
