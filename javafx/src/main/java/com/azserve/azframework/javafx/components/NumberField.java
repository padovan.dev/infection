package com.azserve.azframework.javafx.components;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.regex.Pattern;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.common.lang.NumberUtils;
import com.azserve.azframework.common.lang.StringUtils;
import com.azserve.azframework.javafx.property.BigDecimalPropertyBase;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.IntegerPropertyBase;
import javafx.geometry.Pos;

public class NumberField extends ValueField<BigDecimal> {

	private static final char DECIMAL_SEPARATOR = DecimalFormatSymbols.getInstance().getDecimalSeparator();
	private static final Pattern PATTERN = Pattern.compile("^[-]{0,1}\\d*[,\\.]?\\d*$");

	private final NumberFormat numberFormatThousand;
	private final NumberFormat numberFormat;

	private final IntegerProperty decimalsProperty = new DecimalProperty();

	private final BigDecimalPropertyBase minValueProperty = new BigDecimalPropertyBase(this, "minValue");
	private final BigDecimalPropertyBase maxValueProperty = new BigDecimalPropertyBase(this, "maxValue");

	public NumberField(final int decimals, final int columns) {
		this(decimals);
		this.setPrefColumnCount(columns);
	}

	public NumberField(final int decimals) {
		super();

		this.setAlignment(Pos.CENTER_RIGHT);
		this.numberFormat = new DecimalFormat(NumberUtils.NumberPattern.TWO_DIGITS.getPattern());
		this.numberFormatThousand = new DecimalFormat(NumberUtils.NumberPattern.TWO_DIGITS_THOUSANDS_SEPARATOR.getPattern());
		this.prepareNumberFormat(decimals);
		this.decimalsProperty.set(decimals);

		this.decimalsProperty.addListener((obs, oldValue, newValue) -> {
			if (oldValue.intValue() != newValue.intValue()) {
				this.prepareNumberFormat(newValue.intValue());
				this.updateValue(newValue.intValue());
			}
		});

		this.minValueProperty.addListener((obs, old, value) -> {
			if (value != null) {
				this.setValue(this.fix(this.getValue()));
			}
		});
		this.maxValueProperty.addListener((obs, old, value) -> {
			if (value != null) {
				this.setValue(this.fix(this.getValue()));
			}
		});

		this.focusedProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue.booleanValue()) {
				this.forceUpdateText(NumberUtils.format(this.getValue(), this.numberFormat));
			}
		});
	}

	public IntegerProperty decimalsProperty() {
		return this.decimalsProperty;
	}

	public int getDecimals() {
		return this.decimalsProperty.get();
	}

	public void setDecimals(final int decimals) {
		this.decimalsProperty.set(decimals);
	}

	//

	public BigDecimalPropertyBase minValueProperty() {
		return this.minValueProperty;
	}

	public BigDecimal getMinValue() {
		return this.minValueProperty.get();
	}

	public void setMinValue(final BigDecimal value) {
		this.minValueProperty.set(value);
	}

	//

	public BigDecimalPropertyBase maxValueProperty() {
		return this.maxValueProperty;
	}

	public BigDecimal getMaxValue() {
		return this.maxValueProperty.get();
	}

	public void setMaxValue(final BigDecimal value) {
		this.maxValueProperty.set(value);
	}

	public Integer getIntegerValue() {
		return NumberUtils.toInteger(this.getValue());
	}

	public Double getDoubleValue() {
		return NumberUtils.toDouble(this.getValue());
	}

	public void setIntegerValue(final Integer value) {
		this.setValue(value == null ? null : BigDecimal.valueOf(value.intValue()));
	}

	public void setDoubleValue(final Double value) {
		this.setValue(value == null ? null : BigDecimal.valueOf(value.doubleValue()).setScale(this.decimalsProperty.get(), RoundingMode.HALF_UP));
	}

	private void prepareNumberFormat(final int decimals) {
		this.numberFormat.setMaximumFractionDigits(decimals);
		this.numberFormat.setMinimumFractionDigits(decimals);
		this.numberFormatThousand.setMaximumFractionDigits(decimals);
		this.numberFormatThousand.setMinimumFractionDigits(decimals);
	}

	private void updateValue(final int decimals) {
		final BigDecimal value = this.valueProperty().get();
		if (value != null) {
			this.valueProperty().set(value.setScale(decimals, RoundingMode.HALF_UP));
		}
	}

	@Override
	protected @NonNull String convertNewInput(final @NonNull String text) {
		// FIX per il punto del tastierino numerico
		return text.replace('.', DECIMAL_SEPARATOR);
	}

	@Override
	protected BigDecimal fix(final BigDecimal value) {
		if (value == null) {
			return null;
		}
		final BigDecimal minValue = this.minValueProperty.get();
		if (minValue != null && minValue.compareTo(value) == 1) {
			return null;
		}
		final BigDecimal maxValue = this.maxValueProperty.get();
		if (maxValue != null && maxValue.compareTo(value) == -1) {
			return null;
		}
		return super.fix(value);
	}

	@Override
	protected BigDecimal fromString(final String value) {
		if (StringUtils.isNotNullNorEmpty(value)) {
			try {
				final BigDecimal bd = NumberUtils.parseBigDecimal(value.trim());
				return bd.setScale(this.decimalsProperty.get(), RoundingMode.HALF_UP);
			} catch (final @SuppressWarnings("unused") Exception nfex) {
				//
			}
		}
		return null;
	}

	@Override
	protected String toString(final BigDecimal value) {
		return value == null ? "" : NumberUtils.format(value.setScale(this.decimalsProperty.get(), RoundingMode.HALF_UP), this.numberFormatThousand);
	}

	@Override
	protected boolean checkUserInput(final String text) {
		final boolean b = text == null || PATTERN.matcher(text).matches();
		return b;
	}

	private final class DecimalProperty extends IntegerPropertyBase {

		@Override
		public Object getBean() {
			return NumberField.this;
		}

		@Override
		public String getName() {
			return "decimals";
		}

		@Override
		public void set(final int newValue) {
			if (newValue < 0) {
				throw new IllegalArgumentException("Numero di decimali negativo");
			}
			super.set(newValue);
		}
	}

}
