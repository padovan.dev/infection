package com.azserve.azframework.javafx.components;

import javafx.beans.property.Property;

public interface SimpleBindable<T> extends Bindable<T, T> {

	@Override
	Property<T> valueProperty();

	@Override
	void setReadOnly(boolean readOnly);

	@Override
	default T getValue() {
		return this.valueProperty().getValue();
	}

	@Override
	default void setValue(final T value) {
		this.valueProperty().setValue(value);
	}

	@Override
	default T wrapValue(final T value) {
		return value;
	}

	@Override
	default T unwrapValue(final T wrappedValue) {
		return wrappedValue;
	}
}
