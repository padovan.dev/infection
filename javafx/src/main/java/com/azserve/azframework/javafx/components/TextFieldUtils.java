package com.azserve.azframework.javafx.components;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import com.azserve.azframework.common.lang.StringUtils;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TextInputControl;
import javafx.util.StringConverter;

final class TextFieldUtils {

	@FunctionalInterface
	interface TriConsumer<T> {

		void accept(int i1, int i2, T t);
	}

	interface MaxLength {

		IntegerProperty maxLengthProperty();
	}

	static class Stato<T> {

		final ObjectProperty<T> valueProperty = new SimpleObjectProperty<>();

		/** Indica se il testo corrisponde già al valore formattato */
		boolean updatingText = false;

		/** Indica se il testo è stato inserito dall'utente */
		boolean typing = false;

		/** Indica se forza l'update del testo nel componente */
		boolean forceUpdateText = false;
	}

	static void setReadOnly(final boolean readOnly, final TextInputControl control) {
		control.setEditable(!readOnly);
		control.setFocusTraversable(!readOnly);
	}

	static <T, C extends TextInputControl & SimpleBindable<T>> void initDefaults(final C control, final Stato<T> stato, final Function<String, T> fromString, final Function<T, String> toString, final Function<T, T> fix) {
		// alla perdita del focus aggiorno il testo a partire dal valore
		control.focusedProperty().addListener((e1, e2, e3) -> {
			if (e2.booleanValue()) {
				updateText(control, stato, toString);
			}
		});

		stato.valueProperty.addListener((observable, oldValue, newValue) -> {
			if (!stato.valueProperty.isBound() && !Objects.equals(newValue, oldValue)) {
				stato.valueProperty.set(fix.apply(newValue));
			}
		});

		control.textProperty().bindBidirectional(stato.valueProperty, new StringConverter<T>() {

			@Override
			public T fromString(final String string) {
				return stato.updatingText ? control.getValue() : fromString.apply(string);
			}

			@Override
			public String toString(final T object) {
				return toString.apply(object);
			}
		});

		control.textProperty().addListener((observable, oldValue, newValue) -> {
			if (stato.typing) {
				stato.typing = false;
				return;
			}
			stato.typing = false;
			if (!stato.forceUpdateText) {
				updateText(control, stato, toString);
			}
		});
	}

	static <T> void replaceText(final int start, final int end, final String text, final TextInputControl control, final Stato<T> stato, final Function<String, String> convertNewInput, final Predicate<String> checkUserInput, final TriConsumer<String> replaceText) {
		stato.typing = true;
		final String tmp = StringUtils.nullToEmpty(control.getText());
		final String converted = convertNewInput.apply(text);
		final String tmp1 = tmp.substring(0, start) + converted + tmp.substring(end);
		if (checkUserInput.test(tmp1)) {
			replaceText.accept(start, end, converted);
		}
	}

	static <T> void replaceSelection(final String text, final Stato<T> stato, final Predicate<String> checkUserInput, final Consumer<String> replaceSelection) {
		stato.typing = true;
		if (checkUserInput.test(text)) {
			replaceSelection.accept(text);
		}
	}

	static String fix(final String value, final MaxLength maxLength) {
		return value == null ? null : StringUtils.left(value, maxLength.maxLengthProperty().get());
	}

	static boolean checkUserInput(final String text, final MaxLength maxLength) {
		if (text == null) {
			return true;
		}
		if (text.length() > maxLength.maxLengthProperty().intValue()) {
			return false;
		}
		return true;
	}

	private static <T, C extends TextInputControl & SimpleBindable<T>> void updateText(final C control, final Stato<T> stato, final Function<T, String> toString) {
		stato.updatingText = true;
		control.setText(toString.apply(control.getValue()));
		stato.updatingText = false;
	}

	private TextFieldUtils() {}
}
