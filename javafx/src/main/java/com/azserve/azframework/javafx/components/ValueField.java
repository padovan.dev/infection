package com.azserve.azframework.javafx.components;

import com.azserve.azframework.common.annotation.NonNull;
import com.azserve.azframework.javafx.components.TextFieldUtils.Stato;

import javafx.beans.property.ObjectProperty;
import javafx.scene.control.TextField;

/**
 * Campo di testo per gestire un valore.
 *
 * @param <T> tipo del valore gestito.
 */
public abstract class ValueField<T> extends TextField implements SimpleBindable<T> {

	private final Stato<T> stato = new Stato<>();

	public ValueField() {
		TextFieldUtils.initDefaults(this, this.stato, this::fromString, this::toString, this::fix);
	}

	public ValueField(final T value) {
		this();
		this.setValue(value);
	}

	@Override
	public ObjectProperty<T> valueProperty() {
		return this.stato.valueProperty;
	}

	@Override
	public void setReadOnly(final boolean readOnly) {
		TextFieldUtils.setReadOnly(readOnly, this);
	}

	@Override
	public final void replaceText(final int start, final int end, final String text) {
		TextFieldUtils.replaceText(start, end, text, this, this.stato, this::convertNewInput, this::checkUserInput, super::replaceText);
	}

	@Override
	public final void replaceSelection(final String text) {
		TextFieldUtils.replaceSelection(text, this.stato, this::checkUserInput, super::replaceSelection);
	}

	@NonNull
	protected String convertNewInput(final @NonNull String text) {
		return text;
	}

	protected final void forceUpdateText(final String text) {
		this.stato.forceUpdateText = true;
		this.setText(text);
		this.stato.forceUpdateText = false;
	}

	/**
	 * Effettua controlli sul valore settato e
	 * restituisce il valore corretto.
	 *
	 * @param value valore da controllare.
	 * @return valore corretto o il valore stesso.
	 */
	protected T fix(final T value) {
		return value;
	}

	/**
	 * Verifica se il testo corrente è valido.
	 *
	 * @param text testo corrente.
	 */
	protected abstract boolean checkUserInput(final String text);

	/**
	 * Converte il testo in valore.
	 *
	 * @param text testo corrente.
	 */
	protected abstract T fromString(final String text);

	/**
	 * Rappresenta il valore come testo.
	 *
	 * @param value valore da rappresentare.
	 * @return
	 */
	protected abstract String toString(final T value);
}
