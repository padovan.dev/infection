package com.azserve.azframework.javafx.property;

import java.math.BigDecimal;

import javafx.beans.property.ObjectPropertyBase;

public class BigDecimalPropertyBase extends ObjectPropertyBase<BigDecimal> {

	private static final Object DEFAULT_BEAN = null;
	private static final String DEFAULT_NAME = "";

	private final Object bean;
	private final String name;

	public BigDecimalPropertyBase() {
		this(DEFAULT_BEAN, DEFAULT_NAME);
	}

	public BigDecimalPropertyBase(final BigDecimal initialValue) {
		super(initialValue);
		this.bean = DEFAULT_BEAN;
		this.name = DEFAULT_NAME;
	}

	public BigDecimalPropertyBase(final Object bean, final String name) {
		super();
		this.bean = bean;
		this.name = name == null ? DEFAULT_NAME : name;
	}

	public BigDecimalPropertyBase(final Object bean, final String name, final BigDecimal initialValue) {
		super(initialValue);
		this.bean = bean;
		this.name = name == null ? DEFAULT_NAME : name;
	}

	@Override
	public Object getBean() {
		return this.bean;
	}

	@Override
	public String getName() {
		return this.name;
	}
}
