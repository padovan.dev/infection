package dev.padovan.infection.javafx;

import dev.padovan.infection.javafx.presenter.InfectionPresenter;
import dev.padovan.infection.javafx.ui.InfectionUI;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class InfectionFX extends Application {

	@Override
	public void start(final Stage primaryStage) {
		primaryStage.setTitle("Infection");

		final InfectionUI ui = new InfectionUI();
		primaryStage.setScene(new Scene(ui));
		primaryStage.show();

		final InfectionPresenter presenter = new InfectionPresenter(ui);
		presenter.init();
	}

	public static void main(final String[] args) {
		launch(args);
	}
}