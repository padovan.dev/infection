package dev.padovan.infection.javafx.display;

import java.io.File;

import dev.padovan.infection.core.World;

public interface InfectionDisplay {

	int getInitPopulation();

	void setRecoverCount(int recover);

	void setInfectCount(int infect, int deltaInfect, double deltaInfectPerc);

	void draw(World world);

	void setOnStart(Runnable callback);

	void setOnPause(Runnable callback);

	void setOnReset(Runnable callback);

	int getWorldSize();

	double getMoveIndex();

	double getNeighbourLimit();

	int getAverageRecoveryRound();

	double getContagionPerRound();

	void setOnClose(Runnable callback);

	File getSelectedFile();
}
