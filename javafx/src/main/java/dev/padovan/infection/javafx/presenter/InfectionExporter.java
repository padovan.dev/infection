package dev.padovan.infection.javafx.presenter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import dev.padovan.infection.core.World;
import dev.padovan.infection.core.person.MoveIndex;

public class InfectionExporter {

	final Path file;
	final Workbook wb;
	final Sheet sheet;

	public InfectionExporter(final Path file) {
		this.file = file;
		this.wb = new XSSFWorkbook();
		this.sheet = this.wb.createSheet("Infection");
		final Row header = this.sheet.createRow(0);
		header.createCell(0).setCellValue("Population");
		header.createCell(1).setCellValue("Infected");
		header.createCell(2).setCellValue("Recovered");
		header.createCell(3).setCellValue("Move index");

	}

	public void addRound(final World w, final MoveIndex moveIndex) {
		final Row row = this.sheet.createRow(this.sheet.getLastRowNum() + 1);
		row.createCell(0).setCellValue(w.getPeople().size());
		row.createCell(1).setCellValue(w.infected());
		row.createCell(2).setCellValue(w.recovered());
		row.createCell(3).setCellValue(moveIndex.getMoveIndex());
	}

	public void write() throws FileNotFoundException, IOException {
		try (OutputStream os = new FileOutputStream(this.file.toFile())) {
			this.wb.write(os);
		}
	}

}
