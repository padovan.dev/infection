package dev.padovan.infection.javafx.presenter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import dev.padovan.infection.core.Person;
import dev.padovan.infection.core.Virus;
import dev.padovan.infection.core.World;
import dev.padovan.infection.core.person.MoveIndex;
import dev.padovan.infection.core.person.SimpleEternalPerson;
import dev.padovan.infection.core.virus.SimpleVirus;
import dev.padovan.infection.javafx.display.InfectionDisplay;

public class InfectionPresenter {

	private final InfectionDisplay display;
	private World world;
	private boolean running = false;
	private final MoveIndex moveIndex = new MoveIndex();

	public InfectionPresenter(final InfectionDisplay display) {
		this.display = display;
	}

	public void init() {
		this.display.setOnStart(this::start);
		this.display.setOnPause(this::pause);
		this.display.setOnClose(this::pause);
		this.display.setOnReset(this::reset);
	}

	private void reset() {
		this.pause();
		this.world = null;
		this.display.draw(null);
		this.display.setInfectCount(0, 0, 0);
		this.display.setRecoverCount(0);
	}

	private void start() {
		if (this.running) {
			return;
		}
		this.running = true;
		final Thread t = new Thread(() -> this.play());
		t.start();
	}

	private void pause() {
		this.running = false;
	}

	private void play() {
		this.world = this.getWorld();
		final World w = this.world;
		while (this.running) {
			this.moveIndex.setMoveIndex(this.display.getMoveIndex());
			w.round();
			if (w.infected() == 0) {
				break;
			}
			try {
				Thread.sleep(100);
			} catch (@SuppressWarnings("unused") final InterruptedException ex) {// nothing
			}
		}
	}

	private World getWorld() {
		if (this.world != null) {
			return this.world;
		}
		final int worldSize = this.display.getWorldSize();
		final int worldSizeX = worldSize;
		final int worldSizeY = worldSize;
		final int population = this.display.getInitPopulation();
		final double neighbourLimit = this.display.getNeighbourLimit();
		final int worldArea = worldSizeX * worldSizeY;
		final double density = (double) population / worldArea;
		final int averageRecoveryRound = this.display.getAverageRecoveryRound();
		final double contagionPerRound = this.display.getContagionPerRound();
		final double deathRate = 0;
		final double infectiousness = contagionPerRound / (neighbourLimit * neighbourLimit * Math.PI * density);

		final Virus virus = new SimpleVirus(infectiousness, averageRecoveryRound, false, deathRate / averageRecoveryRound, neighbourLimit);
		final Set<Person> people = new HashSet<>();
		for (int i = 0; i < population; i++) {
			final Person p = new SimpleEternalPerson(i, Math.random() * worldSizeX, Math.random() * worldSizeY, i <= 2, this.moveIndex);
			people.add(p);
		}
		System.out.println("Population : " + population);
		System.out.println("World : " + worldSizeX + " x " + worldSizeY + " = " + worldArea);
		System.out.println("Density : " + density);
		final double virusNeighbourLimit = virus.getNeighbourLimit();
		System.out.println("Avg. neighbour : " + density * virusNeighbourLimit * virusNeighbourLimit * Math.PI);
		System.out.println("R0 : " + density * virusNeighbourLimit * virusNeighbourLimit * Math.PI * infectiousness * averageRecoveryRound);

		final World w = new World(people, virus, worldSizeX, worldSizeY);

		final Path file = Optional.ofNullable(this.display.getSelectedFile()).map(File::toPath).orElse(null);
		if (file != null) {
			final InfectionExporter infectionExporter = new InfectionExporter(file);
			w.addEndRoundListener(e -> {
				infectionExporter.addRound(w, this.moveIndex);
				if (w.infected() == 0) {
					try {
						infectionExporter.write();
					} catch (final IOException ex) {
						ex.printStackTrace();
					}
				}
			});
		}

		w.addEndRoundListener(e -> this.display.draw(w));

		final AtomicLong prevInfect = new AtomicLong(w.infected());

		w.addInfectionsListener(e -> prevInfect.set(e.getPeople().size()));

		w.addEndRoundListener(e -> {
			final World w1 = e.getWorld();
			final long infected = w1.infected();
			if (w1.getRound() == 0) {
				System.out.println("Round\tinfected\trecovered");
			}
			System.out.println(w1.getRound() + "\t" + infected + "\t" + w1.recovered());
			final int deltaInfect = (int) prevInfect.get();
			this.display.setInfectCount((int) infected, deltaInfect, infected == 0 ? 0.0 : 100.0 * deltaInfect / infected);
			prevInfect.set(infected);
			this.display.setRecoverCount((int) w1.recovered());

		});

		return w;
	}
}
