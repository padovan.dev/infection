package dev.padovan.infection.javafx.ui;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import dev.padovan.infection.core.IPerson;
import dev.padovan.infection.core.World;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class CanvasWorld {

	private final Canvas canvas;
	private final World world;

	public CanvasWorld(final Canvas canvas, final World world) {
		this.canvas = canvas;
		this.world = world;
	}

	public void draw() {
		final GraphicsContext gc = this.canvas.getGraphicsContext2D();
		final int pointSize = 4;
		gc.clearRect(0, 0, this.canvas.getWidth(), this.canvas.getHeight());
		if (this.world != null) {
			final Set<IPerson> people = this.world.getPeople();
			final Set<IPerson> s = people.stream().filter(p -> !p.isInfected() & !p.isRecover()).collect(toSet());
			gc.setFill(Color.BLUE);
			for (final IPerson person : s) {
				gc.fillOval(this.getX(person), this.getY(person), pointSize, pointSize);
			}
			final Set<IPerson> i = people.stream().filter(p -> p.isInfected()).collect(toSet());
			gc.setFill(Color.RED);
			for (final IPerson person : i) {
				gc.fillOval(this.getX(person), this.getY(person), pointSize, pointSize);
			}
			final Set<IPerson> r = people.stream().filter(p -> p.isRecover()).collect(toSet());
			gc.setFill(Color.GREEN);
			for (final IPerson person : r) {
				gc.fillOval(this.getX(person), this.getY(person), pointSize, pointSize);
			}
		}
	}

	private double getX(final IPerson person) {
		return CanvasWorld.calc(this.canvas.getWidth(), this.world.getWidth(), person.getX());
	}

	private double getY(final IPerson person) {
		return CanvasWorld.calc(this.canvas.getHeight(), this.world.getHeight(), person.getY());
	}

	private static double calc(final double canvasSize, final double worldSize, final double position) {
		return position * canvasSize / worldSize;
	}
}
