package dev.padovan.infection.javafx.ui;

import java.io.File;
import java.math.BigDecimal;

import com.azserve.azframework.common.lang.NumberUtils;
import com.azserve.azframework.javafx.components.IntegerField;
import com.azserve.azframework.javafx.components.NumberField;

import dev.padovan.infection.core.World;
import dev.padovan.infection.javafx.display.InfectionDisplay;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class InfectionUI extends BorderPane implements InfectionDisplay {

	private final ResizableCanvas canvas = new ResizableCanvas();
	private final Button start = new Button("start");
	private final Button pause = new Button("pause");
	private final Button reset = new Button("reset");
	private final IntegerField initPopulation = new IntegerField(Integer.valueOf(10000));
	private final IntegerField worldSize = new IntegerField(Integer.valueOf(1000));
	private final IntegerField averageRecoveryRound = new IntegerField(Integer.valueOf(10));
	private final NumberField neighbourLimit = new NumberField(4);
	private final NumberField contagionPerRound = new NumberField(4);
	private final Slider moveIndex = new Slider(0, 100, 10);
	private final TextField infectCount = new TextField();
	private final TextField newInfects = new TextField();
	private final TextField newInfectsPerc = new TextField();
	private final TextField recoverCount = new TextField();
	private final Button fileChooser = new Button("Save as");
	private File selectedFile = null;

	public InfectionUI() {
		this.canvas.setWidth(100);
		this.canvas.setHeight(100);
		this.setCenter(this.canvas);
		this.neighbourLimit.setValue(BigDecimal.valueOf(10));
		this.contagionPerRound.setValue(BigDecimal.valueOf(0.4));
		this.moveIndex.setShowTickLabels(true);
		final VBox left = new VBox(
				new Label("Input"),
				new Label("Population"), this.initPopulation,
				new Label("World size"), this.worldSize,
				new Label("Avg recovery per round"), this.averageRecoveryRound,
				new Label("Neighbour limit"), this.neighbourLimit,
				new Label("Contagions per Round"), this.contagionPerRound,
				new Label("Move index"), this.moveIndex,
				new Label("Output"),
				new Label("Infects"), this.infectCount,
				new Label("New infects"), this.newInfects,
				new Label("New infects %"), this.newInfectsPerc,
				new Label("Recovers"), this.recoverCount,
				this.fileChooser,
				this.start,
				this.pause,
				this.reset);
		left.setSpacing(2);
		this.setLeft(left);
		this.infectCount.setEditable(false);
		this.newInfects.setEditable(false);
		this.newInfectsPerc.setEditable(false);
		this.recoverCount.setEditable(false);
		this.fileChooser.setOnAction(e -> {
			final FileChooser fileChooser2 = new FileChooser();
			fileChooser2.getExtensionFilters().addAll(
					new ExtensionFilter("Excel files", "*.xsls"));
			this.selectedFile = fileChooser2.showSaveDialog(this.getScene().getWindow());
		});
	}

	@Override
	public int getInitPopulation() {
		return NumberUtils.removeNull(this.initPopulation.getValue());
	}

	@Override
	public int getWorldSize() {
		return NumberUtils.removeNull(this.worldSize.getValue());
	}

	@Override
	public double getMoveIndex() {
		return this.moveIndex.getValue();
	}

	@Override
	public double getNeighbourLimit() {
		return NumberUtils.removeNull(this.neighbourLimit.getValue()).doubleValue();
	}

	@Override
	public int getAverageRecoveryRound() {
		return NumberUtils.removeNull(this.averageRecoveryRound.getValue());
	}

	@Override
	public double getContagionPerRound() {
		return NumberUtils.removeNull(this.contagionPerRound.getValue()).doubleValue();
	}

	@Override
	public void setOnStart(final Runnable callback) {
		this.start.setOnAction(e -> callback.run());
	}

	@Override
	public void setOnPause(final Runnable callback) {
		this.pause.setOnAction(e -> callback.run());
	}

	@Override
	public void setOnReset(final Runnable callback) {
		this.reset.setOnAction(e -> callback.run());
	}

	@Override
	public void setOnClose(final Runnable callback) {
		this.getScene().getWindow().setOnCloseRequest(e -> callback.run());
	}

	@Override
	public void setRecoverCount(final int recover) {
		Platform.runLater(() -> {
			this.recoverCount.setText(recover + "");
		});
	}

	@Override
	public void setInfectCount(final int infect, final int newInfectsCount, final double newInfectsPercentage) {
		Platform.runLater(() -> {
			this.infectCount.setText(infect + "");
			this.newInfects.setText(newInfectsCount + "");
			this.newInfectsPerc.setText(String.format("%.2f", Double.valueOf(newInfectsPercentage)));
		});
	}

	@Override
	public void draw(final World world) {
		Platform.runLater(() -> {
			new CanvasWorld(this.canvas, world).draw();
		});
	}

	@Override
	public File getSelectedFile() {
		return this.selectedFile;
	}

	private static class ResizableCanvas extends Canvas {

		@Override
		public boolean isResizable() {
			return true;
		}

		@Override
		public double maxHeight(final double width) {
			return Double.POSITIVE_INFINITY;
		}

		@Override
		public double maxWidth(final double height) {
			return Double.POSITIVE_INFINITY;
		}

		@Override
		public double minWidth(final double height) {
			return 1D;
		}

		@Override
		public double minHeight(final double width) {
			return 1D;
		}

		@Override
		public void resize(final double width, final double height) {
			this.setWidth(width);
			this.setHeight(height);
		}
	}

}
